#!/bin/bash

set -e

die () {
    echo $1
    exit 1
}

wait_vm_up () {
    while ! nc -z localhost 22001; do
        sleep 0.1
    done
}

force=$1

[ -f 'itc2023_blank.ova' ] || wget 'https://data.pyrocko.org/scratch/itc2023_blank.ova'


if [ -z "$force" ] ; then
    [ -z "$(vboxmanage list vms | grep '\"itc2023\"')" ] || die 'vm already exists'
else
    [ -z "$(vboxmanage list runningvms | grep '\"itc2023\"')" ] || vboxmanage controlvm 'itc2023' poweroff
    vboxmanage unregistervm 'itc2023' --delete
fi

vboxmanage import itc2023_blank.ova --vsys 0 --vmname itc2023
vboxmanage modifyvm "itc2023" --natpf1 "guestssh,tcp,,22001,,22"

vboxheadless -s itc2023 &
VMPID=$!

wait_vm_up

ssh-add itc_admin

( cd dot-files ; tar -czvf ../itc-seis-dot-files-2023.tar.gz . )

scp -P 22001 -i itc_admin -o 'NoHostAuthenticationForLocalhost yes' lightdm.conf itc-seis-dot-files-2023.tar.gz itc-update install-itc-software.sh itc@localhost:/tmp
#scp -r -P 22001 -i itc_admin -o 'NoHostAuthenticationForLocalhost yes' itc-seis-dataset-2023 itc@localhost:
ssh -t -p 22001 -i itc_admin -o 'NoHostAuthenticationForLocalhost yes' itc@localhost 'bash /tmp/install-itc-software.sh'

vboxmanage controlvm 'itc2023' acpipowerbutton

wait $VMPID

rm -f 'itc2023.ova'
vboxmanage export 'itc2023' --output 'itc2023.ova'

echo 'success'
