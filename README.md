# itc-seis-box

A virtual box image for the moment tensor exercises at the International
Training Courses on Seismology and Seismic Hazard Assessment

This repos is an attempt to automatize part of the setup and configuration of
the VM.

As a base box we need a .ova with a minimal system with

- Make VM disk image large enough, e.g. growable to 30 GB.
- Set RAM to about 4 GB, allow it to use 2 processor cores.
- Current Debian minimal install, no desktop env (this time used Debian 11
  because Virtualbox guest additions were not yet ready for Debian 12).
- User `root`, password `earthquake101`
- User `itc`, password `earthquake101`
- Install sudo
- Add user `itc` to group `sudo`
- Install sshd
- `itc_admin.pub >> /home/itc/.ssh/authorized_keys`
- Install vbox guest additions!
- Remove guest additions ISO before exporting the ova.
