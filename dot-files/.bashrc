
if [ -f "${HOME}/.bashrc_local" ] ; then
    source "${HOME}/.bashrc_local"
fi

if [ -f "${HOME}/.bashrc_interactive" ] ; then
    source "${HOME}/.bashrc_interactive"
fi

if [ -f "${HOME}/.seiscomprc" ] ; then
    source "${HOME}/.seiscomprc"
fi
