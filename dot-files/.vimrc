set incsearch
set nowrapscan
set autoindent
set showcmd
set hlsearch
set nojoinspaces

set hidden

syntax on
filetype plugin indent on

set background=dark
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4

set guioptions-=T
set guioptions-=m
set guioptions-=L
set guioptions-=r

set noeb vb t_vb=

set scrolloff=10

autocmd Syntax * syn match ExtraWhitespace /\(\t\+\|\s\+\%#\@<!$\)/

set colorcolumn=80

set directory-=.

command! -nargs=+ S execute 'silent vimgrep! <args> src/**/*.py source/**/*.rst' | copen


nnoremap <C-s> :w<CR>
inoremap <C-s> <Esc>:w<CR>
inoremap jj <Esc> 
inoremap <C-g> <Esc>

nnoremap <silent><Leader>* :S <C-R><C-W><CR>

nnoremap <silent><Leader>w :s/ \+$//<CR>
nnoremap <silent><Leader>W :%s/ \+$//<CR>

imap <C-a> <kHome>
imap <C-e> <kEnd>

cnoremap <c-n>  <down>
cnoremap <c-p>  <up>

nmap . .`[

nnoremap <silent><Leader>n :put =strftime('%Y-%m-%d')<CR>
nnoremap <silent><Leader>N :put =strftime('%Y-%m-%d %H:%M')<CR>

nnoremap <Leader>, :bp<CR>:bn<CR>
nnoremap <Leader>< :buffers<CR>
nnoremap <Leader>. :bp<CR>
nnoremap <Leader>p :bn<CR>
nnoremap <Leader>y :bn<CR>
nnoremap <Leader>' :bd<CR>


nnoremap <Leader>g :cn<CR>
nnoremap <Leader>G :cp<CR>

nnoremap <Leader>b /, <CR> i<CR><Esc>

nnoremap <Leader>s :s/\([^ ]\)\(<=\\|==\\|>=\\|!=\\|>\\|<\)\([^ ]\)/\1 \2 \3/g<CR>
nnoremap <Leader>S :s/\([^ *\[]\)\(+\\|-\\|\/\\|*\\|\/\/\)\([^ *\]]\)/\1 \2 \3/g<CR>
nnoremap <Leader>a :s/\([^ ]\)\(+=\\|-=\\|\/=\\|*=\\|=\)\([^ ]\)/\1 \2 \3/<CR>


autocmd FileType yaml setlocal shiftwidth=2 tabstop=2

nnoremap [e  :<c-u>execute 'move -1-'. v:count1<cr>
nnoremap ]e  :<c-u>execute 'move +'. v:count1<cr>

vnoremap <silent><Leader>l di`<Esc>pa <>`_<Esc>hhi
vnoremap <silent><Leader>r dmmi::`<Esc>pa`<Esc>`mli

set lbr
