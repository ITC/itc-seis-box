#!/bin/bash

set -e

echo 'export LC_ALL=en_US.UTF-8' | sudo tee /etc/profile.d/locale.sh

source /etc/profile.d/locale.sh

sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get install -y aptitude vim curl virt-what gedit
sudo apt-get install -y task-xfce-desktop lightdm
sudo apt-get remove -y light-locker

sudo cp /tmp/lightdm.conf /etc/lightdm/lightdm.conf

cd $HOME
mkdir -p .local/bin
cp /tmp/itc-update  .local/bin/
chmod a+x .local/bin/itc-update

sudo apt-get install -y make git python3-setuptools python3-pip jupyter

sudo apt-get install -y \
        gmt gmt-gshhg poppler-utils imagemagick

sudo apt-get install -y autoconf gfortran

sudo apt-get install -y jupyter

src="$HOME/itc-seis-software"
[ -d "$src" ] || mkdir "$src"

cd "$src"
[ -d "fomosto-qseis" ] || ( git clone https://git.pyrocko.org/pyrocko/fomosto-qseis.git \
    && cd fomosto-qseis && autoreconf -i && ./configure && make && sudo make install )

cd "$src"
[ -d "pyrocko" ] || ( git clone -b itc2023 https://git.pyrocko.org/pyrocko/pyrocko.git \
    && cd pyrocko && python3 install.py deps system --yes && python3 install.py system --yes )

cd "$src"
[ -d "grond" ] || ( git clone -b itc2023 https://git.pyrocko.org/pyrocko/grond.git \
    && cd grond && sudo pip3 install --no-deps . )

cd "$HOME"
tar -xzvf "/tmp/itc-seis-dot-files-2023.tar.gz"
